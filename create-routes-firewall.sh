#!/bin/bash -x

NODES=`fuel node list | egrep '(controller|compute|storage)' | awk '{ print $1 }'`

for NODE in $NODES
do
ssh node-$NODE << EndOfCommands
  echo "iptables -I INPUT -p tcp -i br-mgmt -s 0/0 -j ACCEPT" >> /etc/rc.local
  echo "route add -net 192.168.0.0 netmask 255.255.0.0 gw 192.168.30.1" >> /etc/rc.local
  echo "route add -net 172.16.0.0 netmask 255.240.0.0 gw 192.168.30.1" >> /etc/rc.local
  echo "route add -net 10.0.0.0 netmask 255.0.0.0 gw 192.168.30.1" >> /etc/rc.local
  iptables -I INPUT -p tcp -i br-mgmt -s 0/0 -j ACCEPT
  route add -net 192.168.0.0 netmask 255.255.0.0 gw 192.168.30.1
  route add -net 172.16.0.0 netmask 255.240.0.0 gw 192.168.30.1
  route add -net 10.0.0.0 netmask 255.0.0.0 gw 192.168.30.1
EndOfCommands
done
