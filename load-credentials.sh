#!/bin/bash

# Get list of nodes
NODES_CONTROLLER=`fuel node list | grep controller  | awk '{ print "node-" $1 }'`
NODES_COMPUTE=`fuel node list | grep compute  | awk '{ print "node-" $1 }'`

for NODE in $NODES_COMPUTE $NODES_CONTROLLER
do
  rsync openrc.sh $NODE:/root/openrc.sh
  ssh $NODE "cat 'source /root/openrc.sh' >> /root/.bashrc"
done
