#!/bin/bash

# Cirros 0.3.4
curl -s http://www.infonas.com/img/cirros-0.3.4-x86_64-disk.img | \
  glance image-create --container-format bare --name cirros-0.3.4 --disk-format qcow2 --progress

# Ubuntu 14.04 - Trusty
curl -s http://www.infonas.com/img/trusty-server-cloudimg-amd64-disk1.img | \
  glance image-create --container-format bare --name ubuntu-trusty --disk-format qcow2 --progress

# Ubuntu 16.04 - Xenial
curl -s http://www.infonas.com/img/xenial-server-cloudimg-amd64-disk1.img | \
  glance image-create --container-format bare --name ubuntu-xenial --disk-format qcow2 --progress
