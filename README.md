## Load Test Environment after Redeployment

The OpenStack environment needs to be set up with basic network related configuration. These scripts when run from the Fuel master node will log in to all Fuel slaves and apply the updates.

The configuration deployments include:

* Add routes for local networks
* Add iptables rule to allow traffic from local networks
* Add credentials file
* Update neutron configuration files for vlans
* Add ifcfg files
* Create ovs bridge and add port
* Create networks
* Create images
* Create projects
* Add key-pairs
