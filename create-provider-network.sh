#!/bin/bash

NEW_VLAN=$1
SUBNET=$2
GATEWAY=$3
POOL_START=$4
POOL_END=$5
EXTERNAL=$6

if [[ -z "$NEW_VLAN" || -z "$SUBNET" || -z "$GATEWAY" || -z "$POOL_START" || -z "$POOL_END" ]]
then
  echo "Parameters Missing"
  echo
  exit 1
fi

NODES_CONTROLLER=`fuel node list | grep controller  | awk '{ print "node-" $1 }'`
NODES_COMPUTE=`fuel node list | grep compute  | awk '{ print "node-" $1 }'`
IFCFG_PROVIDER="/etc/network/interfaces.d/ifcfg-PROVIDER-NETWORKS"

# Compute and Controller Nodes
for NODE in $NODES_CONTROLLER $NODES_COMPUTE
do
ssh $NODE << EndOfContent

  sed -i -r "s/^([#]*)(network_vlan_ranges.*)/\1,vlan$NEW_VLAN:$NEW_VLAN:$NEW_VLAN/g" /etc/neutron/plugins/ml2/ml2_conf.ini
  sed -i -r "s/^([#]*)(bridge_mappings.*)/\1,vlan$NEW_VLAN:br-vlan-$NEW_VLAN/g" /etc/neutron/plugins/ml2/openvswitch_agent.ini

  echo "auto bond0.$NEW_VLAN" >> $IFCFG_PROVIDER
  echo "iface bond0.$NEW_VLAN inet manual" >> $IFCFG_PROVIDER
  echo "vlan-raw-device bond0" >> $IFCFG_PROVIDER
  echo "" >> $IFCFG_PROVIDER
  
  ifup bond0.$NEW_VLAN
  
  ovs-vsctl add-br br-vlan-$NEW_VLAN
  ovs-vsctl add-port br-vlan-$NEW_VLAN bond0.$NEW_VLAN tag=$NEW_VLAN

  service neutron-openvswitch-agent restart
  service openvswitch-switch restart
EndOfContent
done

# Controller Nodes Only
for NODE in $NODES_CONTROLLER
do
ssh $NODE << EndOfCommands
  service neutron-server restart
EndOfCommands
done

# Run network commands on any one of the compute nodes
neutron net-create vlan$NEW_VLAN --shared --provider:network_type vlan --provider:segmentation_id $NEW_VLAN --provider:physical_network vlan$NEW_VLAN --router:external=$EXTERNAL
neutron subnet-create --name subnet-vlan$NEW_VLAN --gateway $GATEWAY --allocation-pool start=$POOL_START,end=$POOL_END --enable-dhcp --dns-nameserver $DNS_SERVEr vlan$NEW_VLAN $SUBNET
