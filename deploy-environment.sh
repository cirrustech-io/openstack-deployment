#!/bin/bash -x

# Exit on error
set -e 

# Create Routes and Firewall rules on hosts
./create-routes-firewall.sh

# Auto-load credentials in root accounts
# ./load-credentials.sh

# Create provider networks
source networks.conf
NETWORKS_COUNT=${#NETWORKS[@]}
# If applications don't exist... download them
for ((i=0;i<$NETWORKS_COUNT;i++))
do
  ./create-provider-network.sh ${!NETWORKS[i]}
done

